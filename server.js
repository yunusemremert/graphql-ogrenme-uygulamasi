const express = require('express');

const { graphqlHTTP } = require('express-graphql');

const myGraphQLSchema = require('./schema');

const app = express();

app.use('/graphql', graphqlHTTP({
    schema : myGraphQLSchema,
    graphiql : true // graphQL ekranından sorgu girme.
}))

app.listen(4000, () => {
    console.log("Bağlandı.")
});