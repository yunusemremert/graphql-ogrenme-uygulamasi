const {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLInt,
    GraphQLString,
    GraphQLList,
    GraphQLNonNull
} = require('graphql');

const fetch = require("node-fetch");

const apiUrl = "http://localhost:3000";

const staffType = new GraphQLObjectType({
    name : "Staff",
    fields : () => ({
        id : {
            type : GraphQLInt
        },
        name : { 
            type : GraphQLString
        },
        email : { 
            type : GraphQLString
        },
        age : { 
            type : GraphQLInt
        }
    })
});

/* const staff = [
    {
        id : 1,
        name : "Yunus Emre",
        email : "yunusemremert@yandex.com",
        age : 25
    },
    {
        id : 2,
        name : "Havva",
        email : "ornek1@yandex.com",
        age : 25
    }
] */

const rootQuery = new GraphQLObjectType({
    name : "RootQuery",
    fields : {
        allStaff : {
            type : GraphQLList(staffType),
            resolve(parent, args) {
                // return staff;

                return fetch(apiUrl + "/staff")
                .then(response => response.json())
                .then(data => data);
            }
        },
        staff : {
            type : staffType,
            args : {
                id : { 
                    type : GraphQLInt
                }
            },
            resolve(parent, args) {
                /* for (let i = 0; i < staff.length; i++) {
                    if (staff[i].id == args.id) {
                        return staff[i];
                    }
                } */

                return fetch(apiUrl + "/staff/" + args.id)
                .then(response => response.json())
                .then(data => data);
            }
        }
    }
})

const mutations = new GraphQLObjectType({
    name : "Mutation",
    fields : {
        addStaff : {
            type : staffType,
            args : {
                name : {
                    type : new GraphQLNonNull(GraphQLString)
                },
                email : {
                    type : new GraphQLNonNull(GraphQLString)
                },
                age : {
                    type : new GraphQLNonNull(GraphQLInt)
                }
            },
            resolve(parent, args) {
                return fetch(apiUrl + "/staff", {
                    method : "POST",
                    headers : {
                        "Content-Type" : "application/json"
                    },
                    body: JSON.stringify({
                        name : args.name,
                        email : args.email,
                        age : args.age
                    })
                })
                .then(response => response.json())
                .then(data => data);
            }
        },
        updateStaff : {
            type : staffType,
            args : {
                id : {
                    type : new GraphQLNonNull(GraphQLInt)
                },
                name : {
                    type : GraphQLString
                },
                email : {
                    type : GraphQLString
                },
                age : {
                    type : GraphQLInt
                }
            },
            resolve(parent, args) {
                return fetch(apiUrl + "/staff/" + args.id, {
                    method : "PATCH",
                    headers : {
                        "Content-Type" : "application/json"
                    },
                    body: JSON.stringify({
                        name : args.name,
                        email : args.email,
                        age : args.age
                    })
                })
                .then(response => response.json())
                .then(data => data);
            }
        },
        deleteStaff : {
            type : staffType,
            args : {
                id : {
                    type : new GraphQLNonNull(GraphQLInt)
                }
            },
            resolve(parent, args) {
                fetch(apiUrl + "/staff/" + args.id, {
                    method : "DELETE"
                });
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query : rootQuery,
    mutation : mutations
});